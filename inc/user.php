<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include_once("db.php");

class User 
{
   var $id_Utente;
   var $username;
   var $nome;
   var $cognome;
   var $password;
   var $session;
   var $ip;
   var $isAdmin;
   
   private $db;
   private $errorString;

	public function __construct()
	{
	   $user="visitatore";
       $this->db = new db();
	}
   
 	public function authenticate()
 	{
		$user = $this->username;
		$pass = $this->password;
		$query="SELECT * FROM `utenti` utente WHERE utente.username='$user' AND utente.password=MD5('$pass');";
		//echo "user: $user  pass: $pass ";
		$result= $this->db->executeQuery($query, $errorString);
		//echo $errorString;
		if(mysql_num_rows($result)==0)
		 return 0;
		
		if(mysql_num_rows($result)==1)
		{
			while($row=mysql_fetch_array($result))
			{
				$this->id_Utente = $row["id_Utente"];
				$this->username = $row["username"];
				$this->nome = $row["nome"];	
				$this->cognome = $row["cognome"];	
				$this->password = $row["password"];	
				$this->isAdmin = $row["isAdmin"];	
			}//end while
		 return 1;
		}	
	    else  return -1;	
    }//end autenticate
}
?>