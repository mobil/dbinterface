<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include_once("conf.php");

/** Ritorna un nuovo array che risulta essere l'aggregato  di tutti gli array  (array di valori come input senza chiavi) */
function array_aggregate()
{
	if (func_num_args() < 2) { return; }
    $arrays = func_get_args();
    $outputArray = array_shift($arrays);
    $remaining = count($arrays);
    for ($i=0; $i<$remaining; $i++)
    {
        $nextArray = $arrays[$i];
        foreach ($nextArray as $value)
        	array_push($outputArray, $value);
    }
    return $outputArray;
}

$runFile = $conf['LOGS_ROOT'];

/***
 *  Funzione usata per scrivere sul file di log 
 */
function debug($author, $message)
{
	global $runFile;
	$fh = fopen($runFile, 'a') or die("can't open file");
	$stringData = "\n[". date("G:i:s") ."] <$author> dice:\n$message";
	fwrite($fh, $stringData);	
	fclose($fh);	
}


/***
 *  Src: http://roshanbh.com.np/2007/12/getting-real-ip-address-in-php.html
 *  Meccanismo per beccare l'indirizzo IP anche delle macchine sotto il proxy (a quel che sembra)
 */
function getClientIpAddress()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    else
      $ip=$_SERVER['REMOTE_ADDR'];
    return $ip;
}

?>