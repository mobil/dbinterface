<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	

	Reference:
	
	thumb.php v1.1
	______________________________________________________________________ 
	Creates a thumbnailed image based on info passed to it via $_GET. 
	
	Images are cached on the server, so server processing overhead is only 
	needed for the first time the script runs on a particular image.
	______________________________________________________________________
	Requires:
		GD Library
	______________________________________________________________________
	Copyright: 
		(C) 2003 Chris Tomlinson. christo@mightystuff.net
		http://mightystuff.net
		
		This library is free software; you can redistribute it and/or
		modify it under the terms of the GNU Lesser General Public
		License as published by the Free Software Foundation; either
		version 2.1 of the License, or (at your option) any later version.
		
		This library is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
		Lesser General Public License for more details.
	
		http://www.gnu.org/copyleft/lesser.txt 
	
*/
@include_once("common.php");

ini_set('memory_limit','64M');



class thumb
{
public $thumb_size_x;
public $thumb_size_y;

private $thumb_path;
private $large_path;
private $image_error;

/**
 * 
 * 
 * @param $thumb_path da passare con la barra finale
 * @param $large_path da passare con la barra finale
 * @return costruisce un oggetto thumb su cui richiamare n volte  il metodo execute per ogni immagine.
 */
	function __construct($large_path, $thumb_path)
	{
	  $this->thumb_path=$thumb_path;
	  $this->large_path=$large_path;
	  $this->thumb_size_x = 200;
	  
	  $this->image_error = "error.png";
	  
	}

/** Basta passare solo il nome del file di errore perchè ci si aspetta 
 *  che tale file sia sempre sulla dir large
 */
	function setErrorFileName($errorFileName)
	{
		$this->image_error = $errorFileName;	
	}


/**
 * Esegue la creazione del thumbnail 
 * @param $file  è solo il nome del file senza path
 * @return 
 */
	function execute($file)
	{
		$thumb_filename =  $this->thumb_path . $file;	
		$filename = $this->large_path . $file;
		$image_error = $this->large_path . $this->image_error;
		debug("execute", "[thumb]: large: $filename;  thumb: $thumb_filename; err: $image_error");
		$thumb_size_x = $this->thumb_size_x;
		
		if (!file_exists($filename))
			return -2;
		# determine php and gd versions
	  	$ver=intval(str_replace(".","",phpversion()));
	    if ($ver>=430)
	        $gd_version=@gd_info();
	        
	    # define the right function for the right image types
	    if (!$image_type_arr = @getimagesize($filename))
			return -1;
	 
	    $image_type=$image_type_arr[2];
	    
	    $image;
	    switch ($image_type)
	    {
		    case 1: # GIF 
		    	$image = imagecreatefromgif ($filename);
			    break;
	    	
	    	case 2: # JPG
		    	$image = @imagecreatefromjpeg ($filename);
			    break;
	    
		    case 3: # PNG
		    	$image = @imagecreatefrompng ($filename);
			    break;    
	    }
	   
	    if (!$image)
	    {
		    # not a valid jpeg file
		    $image = imagecreatefrompng ($image_error);
		    $file_type="png";	    
	    } 
	    
	    
	    # define size of original image	
	    $image_width = imagesx($image);
	    $image_height = imagesy($image);
	    
	    # define size of the thumbnail	
	    if ($thumb_size_x>0)
	    {
		    # define images x AND y
		    $thumb_width = $thumb_size_x;
		    $factor = $image_width/$thumb_size_x;
		    $thumb_height = intval($image_height / $factor); 
		   
	    } 
	   
	    
	    # create the thumbnail
	    if ($image_width < 4000)	//no point in resampling images larger than 4000 pixels wide - too much server processing overhead - a resize is more economical
	    {
		    if (substr_count(strtolower($gd_version['GD Version']), "2.")>0)
		    {
			    //GD 2.0 
			    $thumbnail = ImageCreateTrueColor($thumb_width, $thumb_height);
			    imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width, $image_height);
		    } 
		    else 
		    {
			    //GD 1.0 
			    $thumbnail = imagecreate($thumb_width, $thumb_height);
			    imagecopyresized($thumbnail, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width, $image_height);
		    }	
	    } 
	    else 
	    {
		    if (substr_count(strtolower($gd_version['GD Version']), "2.")>0)
		    {
			    # GD 2.0 
			    $thumbnail = ImageCreateTrueColor($thumb_width, $thumb_height);
			    imagecopyresized($thumbnail, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width, $image_height);
		    } 
		    else
		    {
			    # GD 1.0 
			    $thumbnail = imagecreate($thumb_width, $thumb_height);
			    imagecopyresized($thumbnail, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $image_width, $image_height);
		    }
	    }
	    
	   
	    switch ($image_type)
	    {
		    case 1:	# GIF 
				imagegif($thumbnail,$thumb_filename);		    
				break;
	    	case 2:	# JPG		    
			    imagejpeg($thumbnail,$thumb_filename, $quality);    
			    break;
		    case 3: # PNG		    
			    imagepng($thumbnail,$thumb_filename);		    
			    break;
	    }
    
	    //clear memory
	    imagedestroy ($image);
	    imagedestroy ($thumbnail);
	    
	    return 1;
    }




	function unlink($file)
	{
		$thumb_filename =  $this->thumb_path . $file;	
		$filename = $this->large_path . $file;		
			
		if (file_exists($thumb_filename))
			unlink($thumb_filename);
		if (file_exists($filename))
			unlink($filename);
	}

}
?>