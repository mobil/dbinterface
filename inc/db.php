<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/
@include_once("conf.php");

define("DATABASE_HOST", $conf['DATABASE_HOST']);
define("DATABASE_LOGIN", $conf['DATABASE_LOGIN']);
define("DATABASE_PASS", $conf['DATABASE_PASS']);
define("DATABASE_NAME",$conf['DATABASE_NAME']);


class db 
{
	
	function executeQuery($query, &$errorString, $connessione=NULL)
	{
		$chiudi = true;
		if(is_null($connessione))
		{
			$connessione= mysql_connect(DATABASE_HOST,DATABASE_LOGIN, DATABASE_PASS) or die(mysql_error());
			mysql_select_db(DATABASE_NAME, $connessione) or die("Errore nella selezione del database");
			
		}
		else
			$chiudi = false;
		$result = mysql_query($query, $connessione);
		$errorCode= mysql_errno($connessione);
		if($errorCode != 0)
			$errorString = "[" . $errorCode . "] : " . mysql_error($connessione) . "\r\n";
		if ($chiudi)
			mysql_close($connessione);
		return $result;
	}

/** Sezione di funzioni dedicate per effettuare query che prevedono transazioni */

	function startTransaction($connessione){	mysql_query("START TRANSACTION", $connessione) ;	}
	function commit($connessione){			mysql_query("COMMIT", $connessione);	mysql_close($connessione);}
	function rollback($connessione){		mysql_query("ROLLBACK", $connessione);	mysql_close($connessione);}
	
	
	/**
	 * Funzione che se fallisce realizza automaticamente il rollback
	 */
	function mysql_queryExecuteWithProblem($query, $connessione, &$errorString)
	{
		$result = mysql_query($query, $connessione);
		$errorCode= mysql_errno($connessione);
		if($errorCode != 0)
		{
			$errorString = "[" . $errorCode . "] : " . mysql_error($connessione) . "\r\n on query:\r\n". $query ;
			$this->rollback($connessione); 
			return true;
		}		
		return false;
	}
	
	public function getConnection()
	{
		$connessione= mysql_connect(DATABASE_HOST,DATABASE_LOGIN, DATABASE_PASS) or die(mysql_error());
		mysql_select_db(DATABASE_NAME, $connessione) or die("Errore nella selezione del database");
		return $connessione;	
	}

}
?>