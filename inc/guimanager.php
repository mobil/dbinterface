<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include_once("db.php");
@include_once("thumb.php");
@include_once("table.php");
@include_once("common.php");
@include_once("conf.php");

define("LARGE_PATH", $conf['IMG_REP_ROOT_LARGE']);
define("THUMB_PATH", $conf['IMG_REP_ROOT_THUMB']);


/** Src:  http://us3.php.net/manual/en/class.domxpath.php */
class xpathExtension
{
	
   public static function getNodes($domDoc, $xpathString) 
   {
        $xp = new DOMXPath($domDoc);
        $xp->registerNamespace('x', 'http://www.w3.org/1999/xhtml');
        $xp->registerNamespace('xhtml', 'http://www.w3.org/1999/xhtml');
        $xp->registerNamespace('i18n', 'http://apache.org/cocoon/i18n/2.1');

        $ret = array();
        $nodes = $xp->query($xpathString);
        foreach ($nodes as $node)
            array_push($ret, $node);        
        return $ret;
    }
}
$registry = array(
	"cataloghi" => 4,
	"marchi" => 4,
	"prodoCataloghi" => 3, 
	"prodoInComposizione" => 3,
	"prodoMarchi" => 3,
	"prodoTags" => 3,
	"prodoTipi" => 3,
	"prodotti"=> 5,
	"tags" => 4,
	"tipi" => 4
);

function cmpTableDelete($tableName_a, $tableName_b)
{
	global $registry;
	//echo "\n<br /> comparo: [$tableName_a, $tableName_b ] = " . ($registry[$tableName_a]  -  $registry[$tableName_b]) ;
    return $registry[$tableName_a]  -  $registry[$tableName_b];
}

function cmpTableCreate($tableName_a, $tableName_b)
{
	global $registry;
	//echo "\n<br /> comparo: [$tableName_a, $tableName_b ] = " . ($registry[$tableName_a]  -  $registry[$tableName_b]) ;
    return $registry[$tableName_b]  -  $registry[$tableName_a];
}






class guimanager
{
//Dispatcher che richiama la funzione opportuna per processare la richiesta
  function process($xmlmessage)
  {  	
  	//debug("gui.process", $xmlmessage);
  	$dom = new DomDocument('1.0'); 
  	//echo "[process] Mi arriva: <textarea rows=\"10\" cols=\"180\"> ".$xmlmessage . "</textarea>";
  	$dom->loadXML($xmlmessage);
  	$ctx = array();
  	/** Richiamo tutti i gestori che effettueranno i loro compiti specifici di prelievo sul dom 
  	 *  e popoleranno $listaquery con tutte le query specifiche */
	$this->creaManager($dom, $ctx);
	$this->aggiornaManager($dom, $ctx);
	$this->cancellaManager($dom, $ctx);
	
	$this->listaManager($dom, $ctx);
	
	$this->raw($dom, $ctx);
		
	$resp = $this->ctxProcess($ctx);
  	return $resp;
    //return "NOT IMPLEMENTED YET!";
  }
  
  private function creaManager($dom, &$ctx)
  {
  	$xpathStringCrea= "//request/op[@id='crea']/item";
  	$domNodeList = xpathExtension::getNodes($dom, $xpathStringCrea);
  	$tableList = array();
  	$thumbnailize = array();
  	$table = NULL;
  	$entrato = false;
  	/** Ciclo su tutti gli item*/
	foreach($domNodeList as $domNode)
	{

		$tableName = $domNode->attributes->getNamedItem("table")->nodeValue;
		if($table == NULL )		
			$table = new table($tableName);			
		elseif($tableName != $table->getName())
		{
			$tableList[$table->getName()] = $table;
			$table = new table($tableName);			
		}
		$row = $table->getCols();
		/** Ciclo sulle colonne della tabella e 
		 *  mi vado a cercare se esistono attributi che mi interessano*/		
		foreach($row as $key=>$value)
		{
			if($domNode->attributes->getNamedItem($key)  != NULL)
			{
				$row[$key] = $domNode->attributes->getNamedItem($key)->nodeValue;
				if($table->getName() == "prodotti" && $key == "img")
					array_push($thumbnailize, $domNode->attributes->getNamedItem($key)->nodeValue);

			}
				
		}
		$table->insert($row);
		$entrato = true;		
	}
	if($entrato)
		$tableList[$table->getName()] = $table;	
 	uksort($tableList, "cmpTableCreate");
	$ctx['insert'] = $tableList;
	$ctx['thumbnailize'] = $thumbnailize;
  }
  
  private function aggiornaManager($dom, &$ctx)
  {
  	$xpathString= "//request/op[@id='aggiorna']/item";
  	$domNodeList = xpathExtension::getNodes($dom, $xpathString);
  	$entrato = false;
  	$tableList = array();
  	/** Ciclo su tutti gli item*/
  	foreach($domNodeList as $domNode)
	{
		$tableName = $domNode->attributes->getNamedItem("table")->nodeValue;
		$where = $domNode->attributes->getNamedItem("where")->nodeValue;
		if($table == NULL )		
			$table = new table($tableName);			
		elseif($tableName != $table->getName())
		{
			$tableList[$table->getName()] = $table;
			$table = new table($tableName);			
		}
		$row = $table->getCols();
		/** Ciclo sulle colonne della tabella e 
		 *  mi vado a cercare se esistono attributi che mi interessano*/		
		foreach($row as $key=>$value)
		{
			if($domNode->attributes->getNamedItem($key)  != NULL)
				$row[$key] = $domNode->attributes->getNamedItem($key)->nodeValue;
		}
		$table->update($row, $where);
		$entrato = true;
	}
	if($entrato)
		$tableList[$table->getName()] = $table;
	uksort($tableList, "cmpTableCreate");
	
	$ctx['update'] = $tableList;
  }
  
  private function cancellaManager($dom, &$ctx)
  {
  	$xpathString= "//request/op[@id='cancella']/item";
  	$domNodeList = xpathExtension::getNodes($dom, $xpathString);
  	$entrato = false;
  	$tableList = array();
  	$delImgList = array();
  	/** Ciclo su tutti gli item*/
  	foreach($domNodeList as $domNode)
	{
		$tableName = $domNode->attributes->getNamedItem("table")->nodeValue;
		$where = $domNode->attributes->getNamedItem("where")->nodeValue;
		if($table == NULL )		
			$table = new table($tableName);			
		elseif($tableName != $table->getName())
		{
			$tableList[$table->getName()] = $table;
			$table = new table($tableName);			
		}
		$table->delete($where);
		if($table->getName()=="prodotti")
		{
			$imgName = $table->getProductImg($where);
			if($imgName != "")
				array_push($delImgList, $imgName );
		}
		$entrato = true;
	}
	if($entrato)
		$tableList[$table->getName()] = $table;
	uksort($tableList, "cmpTableDelete");
	$ctx['delete'] = $tableList;
	$ctx['deleteImgList'] = $delImgList;
  }
  
  private function listaManager($dom, &$ctx)
  {
  	$xpathString= "//request/op[@id='view']/item";
  	$domNodeList = xpathExtension::getNodes($dom, $xpathString);
  	$entrato = false;
  	$tableList = array();
  	foreach($domNodeList as $domNode)
	{
		$tableName = $domNode->attributes->getNamedItem("table")->nodeValue;
		$where = "";
		if($domNode->attributes->getNamedItem("where")  != NULL)
			$where = $domNode->attributes->getNamedItem("where")->nodeValue;
		
		if($table == NULL )		
			$table = new table($tableName);			
		elseif($tableName != $table->getName())
		{
			$tableList[$table->getName()] = $table;
			$table = new table($tableName);
		}		
		$table->view($where);
		$entrato = true;
	}
	if($entrato)
		$tableList[$table->getName()] = $table;
	$ctx['list'] = $tableList;
  }
  
  
  private function raw($dom, &$ctx)
  {
  	$xpathString= "//request/op[@id='raw']/item";
  	$domNodeList = xpathExtension::getNodes($dom, $xpathString);
  	$queryList = array();
  	foreach($domNodeList as $domNode)
	{
		$id = $domNode->attributes->getNamedItem("id")->nodeValue;
		$query = $domNode->attributes->getNamedItem("q")->nodeValue;				
		
		array_push($queryList, $query);
	}
	$ctx['raw'] = $queryList;
  }
  
 
  /** Metodo usato per processare tutte le richieste al DB*/
  private function ctxProcess(&$ctx)
  {
  	$db = new db();
  	$connessione= $db->getConnection();
  	$db->startTransaction($connessione);
  	$erroMessage="";
  	
  	$tablesOfList = $ctx['list'];
  	$tablesOfDelete = $ctx['delete'];
  	$tablesOfInsert = $ctx['insert'];
  	$tableOfUpdate = $ctx['update'];
  	$thumbnailize =  $ctx['thumbnailize'];
  	$delImgList = $ctx['deleteImgList'];
  	
  	
  	$response = "<response>";
  	if(count($tablesOfList)>0)
  	{
  		$patterns= array();
		$replacements = array();
		$patterns[0] = '/"/';
		$replacements[0] = "&quot;";
		
  		$responseOfList = "<op id='view'>";
  		foreach($tablesOfList as $name=>$table)
  		{
  			$qry_s = $table->serialize();
  			$cols = $table->getCols();
  			$counter=0;
  			foreach($qry_s as $qry)
  			{
  				$result = $db->executeQuery($qry, $erroMessage, $connessione); 
  				while($row=mysql_fetch_array($result))
  				{
  					$responseOfList .= "<item table='".$table->getName() ."' where=\"" . $table->getWhere($row) . "\" " ; 
  					foreach($cols as $cKey=>$cVal)  					
  						$responseOfList .= " ". $cKey . "=\"". preg_replace($patterns, $replacements, $row[$cKey]) ."\"" ;
  					$responseOfList .= " />";	
  				}
  			}
  		}
  		$responseOfList .= "</op>";
  		$response .= $responseOfList;  			
  	}  	
  	/* Eseguo in sequenza tutte le insert, tutte le update , tutte le delete */

  	if(count($tablesOfInsert)>0)
  	{
  		$responseOfList = "<op id='crea'>";
  		$i=0;
	  	foreach($tablesOfInsert as $name=>$table)
	  	{
	  		$qry_s = $table->serialize();
	  		foreach($qry_s as $qry)
  			{
		  		if($db->mysql_queryExecuteWithProblem($qry, $connessione, $erroMessage))
	  			{
		  			$response = "<response><op id='crea'><item id='error' m='$erroMessage (eseguendo $qry) ' /></op></response>";
					return $response;   			
		  		}					
				$i++;
  			}
	  	}
	  	if($i > 0)
	  		$responseOfList .= "<item id='ok' m='Eseguite $i INSERT con successo' />";
	  	$responseOfList .= "</op>";	  	
  		$response .= $responseOfList;
  	}

  	/** UPDATE */
    if(count($tableOfUpdate)>0)
  	{
  		$responseOfList = "<op id='aggiorna'>";
  		$i=0;
	  	foreach($tableOfUpdate as $name=>$table)
	  	{
	  		$qry_s = $table->serialize();
	  		foreach($qry_s as $qry)
  			{
		  		if($db->mysql_queryExecuteWithProblem($qry, $connessione, $erroMessage))
	  			{
		  			$response = "<response><op id='aggiorna'><item id='error' m='$erroMessage (eseguendo $qry) ' /></op></response>";
					return $response;   			
		  		}					
				$i++;
  			}
	  	}
	  	if($i > 0)
	  		$responseOfList .= "<item id='ok' m='Eseguite $i UPDATE con successo' />";
	  	$responseOfList .= "</op>";	  	
  		$response .= $responseOfList;
  	}

  	/** DELETE */
   if(count($tablesOfDelete)>0)
   {
  		$responseOfList = "<op id='cancella'>";
  		$i=0;
	  	foreach($tablesOfDelete as $name=>$table)
	  	{
	  		$qry_s = $table->serialize();
	  		foreach($qry_s as $qry)
  			{
		  		if($db->mysql_queryExecuteWithProblem($qry, $connessione, $erroMessage))
	  			{
		  			$response = "<response><op id='cancella'><item id='error' m='$erroMessage (eseguendo $qry) ' /></op></response>";
					return $response;   			
		  		}					
				$i++;
  			}
	  	}
	  	if($i > 0)
	  		$responseOfList .= "<item id='ok' m='Eseguite $i DELETE con successo' />";
	  	$responseOfList .= "</op>";	  	
  		$response .= $responseOfList;
   }
  	
  	/* Eseguo le raw query */
  	if(count($ctx['raw'])>0)
  	{
  		$patterns= array();
		$replacements = array();
		$patterns[0] = '/"/';
		$replacements[0] = "&quot;";
		
  		$responseOfList = "<op id='raw'>";
	  	foreach($ctx['raw'] as $qry)
	  	{
	  		$result = $db->executeQuery($qry, $erroMessage, $connessione); 
	  		while($row=mysql_fetch_array($result))
	  		{
	  			$responseOfList .= "<item table='unknown' query=\"" . $qry  . "\"" ; 
	  			foreach($row as $cKey=>$cVal)
	  				if(!is_int($cKey))  					
	  					$responseOfList .= " ". $cKey . "=\"". preg_replace($patterns, $replacements, $cVal)  ."\"" ;
	  			$responseOfList .= " />";	
	  		}
	  	}
	  	$responseOfList .= "</op>";
	  	$response .= $responseOfList;  
  	}  

  	/** 
  	 * Operazioni sul file system
  	 * [1] Creazione di thumbnail  
  	 * [2] Cancellazione di immagini e thumbnail
  	 */
  	$thumbT = new thumb(LARGE_PATH, THUMB_PATH);

  	//[1] TODO: andrebbe considerato cosa succede se execute torna un valore negativo
  	$msg="";
  	$nick="ctxProcess";
  	foreach($thumbnailize as $imgFileName)
  	{	
		$res= $thumbT->execute($imgFileName);
		
		
		switch($res)
		{
			case -1: $msg= "[Thumbneilize]: Non riesco a richiamare getimagesize!"; break;
			case -2: $msg="[Thumbneilize]: Il file $imgFileName non esiste nella posizione ". LARGE_PATH ; break;
		}
		if($msg != "")
		{
			debug($nick,$msg);
			$msg= "";
		}
			
  	}
	//[2]
	foreach($delImgList as $imgFileName)
		$thumbT->unlink($imgFileName);	

	
		
  	$db->commit($connessione);  	
  	$response .= "</response>";
	return $response;
  }  
}//end class

?>

