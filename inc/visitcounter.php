<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include("db.php");
/*
defaultUser: visitatore
contatoreVisite:
Le visite sono giornaliere ovvero ad un utente viene conteggiata al massimo una visita al giorno su una pagina.
IP, userid, pagina sono elementi in chiave
 IP |  userid | pagina  | data | contatoreVisite 

*/

class visitcounter 
{
  private $user;
  private $db;
  private $errorString;

  public function __construct()
  {
       $user="visitatore";
       $db = new db();
       $errorString="";
  }
  

  public function setUser($userId)
  {
    if(strlen($userId) > 0)
      $user = $userId;
  }
  /**
    @return il numero di visite effettuate dall'utente user sulla pagina corrente
  */
  function update()
  {
    //$today = date("Y-m-d");
			    //mese    //giorno   //anno
    $today = mktime(0, 0, 0, date("m"), date("d"), date("Y"), 0);
  
    $var = $_SERVER['PHP_SELF'];
    if(strlen($_SERVER['PHP_SELF']) > 64)
      $var = substr($_SERVER['PHP_SELF'], -64);
    $user = "visitatore";
  
    $query = "SELECT * FROM `contatori` ".
    " WHERE `ipv4` = '" .  $_SERVER['REMOTE_ADDR'] . "' ".
    " and `userid`='$user' ".
    " and `pagina`='$var' " ;
  
    $result = $db->executeQuery($query, $errorString);
    $queryDaEffettuare="";
    if(mysql_num_rows($result)>0)
    {
       while($row=mysql_fetch_array($result))
       {
	  $dtAr =  explode("-", $row['data']);
	  $dbDate = mktime(0, 0, 0, $dtAr[1], $dtAr[2], $dtAr[0], 0);
	  if(($today - $dbDate)/(60*60*24) > 0)
	  {
	    //Faccio l'aggiornamento del contatore e della data odierna. 
	    $contatore = $row['contatoreVisite'] + 1;
	    $query = "UPDATE  `contatori`  SET `data` = '" . date("Y-m-d") . "', `contatoreVisite`='$contatore'".
	      " WHERE `ipv4` = '" .  $_SERVER['REMOTE_ADDR'] . "' ".
	      " and `userid`='$user' ".
	      " and `pagina`='$var' " ;
	    $db->executeQuery($query, $errorString);       
	    return $contatore;
	  }
	  else
	    return $row['contatoreVisite'];
       }//end while
    }
    else
    {
	//Primo inserimento
	$cont= 1;
	$query="INSERT INTO `mqsweb`.`contatori` ( `ipv4` ,`ipv6` , `userid` , `pagina` , `data` , `contatoreVisite`) " .
      " VALUES ('" . $_SERVER['REMOTE_ADDR'] . "', '--------------', '$user', '$var', '" . date("Y-m-d") . "', '$cont')";
	$db->executeQuery($query, $errorString);
	return $cont;  
    }
  }

  function getTodayVisitors($page = 'home')
  {
      $query = 	" SELECT count( * ) as visitors" .
		" FROM `contatori` " .
		" WHERE `data` = '" . date("Y-m-d") . "' ";

      $result = $db->executeQuery($query, $errorString);
      $row=mysql_fetch_array($result);
      return $row['visitors'];
  }
}
