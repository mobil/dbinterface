<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include_once("./db.php");
@include_once("./common.php");

class table
{
	private $name;
	private $cols;
	
	private $insertString;
	private $firstInsert;
	private $updateStringArr;
	private $deleteStringArr;
	private $viewString;
	
	function __construct($tableName)
	{
		$this->name = $tableName;
		$this->inizialize();		
	}

	private function inizialize()
	{
		$query = "SHOW FIELDS FROM `". $this->name . "`";
		$db = new db();		
		$errorString = "";
		$result = $db->executeQuery($query, $errorString);	
			
		while($row=mysql_fetch_array($result))
		{	
			$key = $row[0];
			$this->cols[$key] = "NULL_0";
		}
		$this->updateStringArr = array();
		$this->deleteStringArr = array();
		$this->viewString = "";
		
		$this->insertString = "INSERT INTO `" . $this->name . "` (";
		$primo= true;
		foreach($this->cols as $key=>$value)
		{
			if($primo)
				$primo = false;
			else
				$this->insertString .= ", ";
			$this->insertString .= "`" . $key . "`" ;		
		}
		$this->insertString .=  ") VALUES ";
		$this->firstInsert = true;
		
	}
	
	public function getName()
	{
		return $this->name;	
	}
	
	public function getCols()
	{
		return $this->cols;
	}
	
	public function insert($row)
	{
		/*$query = "INSERT INTO `mqsweb`.`tags` (
					`idTags` ,
					`nomeTag`
					)
					VALUES (
					NULL , 'aaa'
					), (
					NULL , 'bbbb'
					)";*/
		if ($this->firstInsert)
			$this->firstInsert = false;
		else
			$this->insertString .= ", " ;
			
		$item = "( ";
		$primo= true;
		
		foreach($row as $key=>$value)
		{
			if($value == "NULL_0")
				$value = "NULL";
								
			if($primo)
				$primo = false;
			else
				$item .= ", ";
									
			if($value != "NULL")
				$item .= "'" . addslashes($value) . "'";
			else
			 	$item .=  $value ;
		}
		$item .= ")";
		$this->insertString .= $item;
		
	}
	
	public function update($row, $where)
	{
		$query = "UPDATE ".$this->name. " SET ";
		$primo= true;
		foreach($row as $key=>$value)
		{
			if($value != "NULL_0")
			{
				if($primo)
					$primo = false;
				else
					$query .= ", ";
					
				if($value != "NULL")
					$query .= "`" . $key . "`='" . addslashes($value) . "'";
				else
				 	$query .= "`" . $key . "`=" . $value ;
			}
		}
		$query .= "  WHERE ". $where; 
		array_push($this->updateStringArr, $query);
		
	}
	

	public function delete($where)
	{
		
		switch($this->name)
		{
			case "prodotti":
				$replacedWhere = str_replace("id", "idProd", $where);
				debug("table.delete", "REPLACEDWHERE: ".$replacedWhere);
				//Se è o NON è una composizione
				$query = "DELETE FROM `prodoCataloghi` WHERE " . $replacedWhere ;
				array_push($this->deleteStringArr, $query);	

				$query = "DELETE FROM `prodoInComposizione` WHERE " . $replacedWhere ;
				array_push($this->deleteStringArr, $query);
				
				$query = "DELETE FROM `prodoMarchi` WHERE " . $replacedWhere ;
				array_push($this->deleteStringArr, $query);
				
				$query = "DELETE FROM `prodoTags` WHERE " . $replacedWhere ;
				array_push($this->deleteStringArr, $query);
				
				$query = "DELETE FROM `prodoTipi` WHERE " . $replacedWhere ;
				array_push($this->deleteStringArr, $query);

				//Se è una composizione 
				/* 
				 * In realtà lo faccio sempre per evitare ulteriore calcolo dato che i due indici della
				 * tabella insistono sempre sulla tabella prodotti quindi si escluderebbero loro stessi a vicenda 
				 */
				$replacedWhere2 = str_replace("id", "idComposizione", $where);
				$query = "DELETE FROM `prodoInComposizione` WHERE " . $replacedWhere2 ;
				array_push($this->deleteStringArr, $query); 
			
				break;
				
			case "cataloghi":
				$query = "DELETE FROM `prodoCataloghi` WHERE " . $where ;
				array_push($this->deleteStringArr, $query);
				break;
				
			case "marchi":
				$query = "DELETE FROM `prodoMarchi` WHERE " . $where ;
				array_push($this->deleteStringArr, $query);
				break;
				
			case "tags":
				$query = "DELETE FROM `prodoTags` WHERE " . $where ;
				array_push($this->deleteStringArr, $query);
				break;
			
			case "tipi":
				$query = "DELETE FROM `prodoTipi` WHERE " . $where ;
				array_push($this->deleteStringArr, $query);
				break;
		}

		$query = "DELETE FROM `" . $this->name . "` WHERE " . $where ;
		array_push($this->deleteStringArr, $query);
	}
	
	public function getProductImg($where)
	{
		$nameFile="";
		if( $this->name == "prodotti")
		{
			$query = "SELECT img FROM  `" . $this->name . "` WHERE " . $where ;
			$db = new db();		
			$errorString = "";
			$result = $db->executeQuery($query, $errorString);	
				
			while($row=mysql_fetch_array($result))
				$nameFile = $row["img"];
		}
		return $nameFile;
		
	}

	public function view($where)	
	{
		$this->viewString = "SELECT * FROM `". $this->name . "` ";
				if(strlen($where) > 0)
				 	$this->viewString .= " WHERE ". $where;
		
	}
	
	
	/** Esegue le query effettive sul DB
	 *  La serialize va sempre richiamata in regime di transazione 
	 * 
	 * @return String il gruppo di query da applicare alla tabella*/	
	public function serialize()
	{
		$arrResult = array();
		if(!$this->firstInsert)
			array_push($arrResult, $this->insertString);
		$arrResult = array_aggregate($arrResult, $this->updateStringArr, $this->deleteStringArr );		
		if(strlen($this->viewString) > 0)
			array_push($arrResult, $this->viewString);			
		return $arrResult;			
	}
	
	
	/**
	 * In attesa di un'implementazione + generale nn rimane altro che fare
	 * una lista di casi da censire di volta in volta
	 * @return la stringa del where  da appiccicare nelle liste
	 */
	public function getWhere($row)
	{
		$where = "";
		$key1="";
		$key2="";
		
		switch(strtolower($this->name))
		{
			case "cataloghi":
				$key1 = "idCatalogo";
				$where = " " . $key1 ."='". $row[$key1] ."' ";				
				break;
				
			case "marchi":
				$key1 = "idMarchio";
				$where = " " . $key1 ."='". $row[$key1] ."' ";				
				break;
				
			case "prodocataloghi":
				$key1 = "idProd";
				$key2 ="idCatalogo";				
				$where = " " . $key1 ."='". $row[$key1] ."' AND  ". $key2 ."='". $row[$key2] ."' ";				
				break;
						
			case "prodoincomposizione":
				$key1 = "idProd";
				$key2 ="idComposizione";				
				$where = " " . $key1 ."='". $row[$key1] ."' AND  ". $key2 ."='". $row[$key2] ."' ";				
				break;

			case "prodomarchi":
				$key1 = "idProd";
				$key2 ="idMarchio";				
				$where = " " . $key1 ."='". $row[$key1] ."' AND  ". $key2 ."='". $row[$key2] ."' ";				
				break;
			
			case "prodotags":
				$key1 = "idProd";
				$key2 ="idTags";				
				$where = " " . $key1 ."='". $row[$key1] ."' AND  ". $key2 ."='". $row[$key2] ."' ";				
				break;
				
			case "prodotipi":
				$key1 = "idProd";
				$key2 ="idTipo";				
				$where = " " . $key1 ."='". $row[$key1] ."' AND  ". $key2 ."='". $row[$key2] ."' ";				
				break;
			
			case "prodotti":
				$key1 = "id";
				$where = " " . $key1 ."='". $row[$key1] ."' ";				
				break;

			case "tags":
				$key1 = "idTags";
				$where = " " . $key1 ."='". $row[$key1] ."' ";				
				break;
			
			case "tipi":
				$key1 = "idTipo";
				$where = " " . $key1 ."='". $row[$key1] ."' ";				
				break;
			
		}
		
		return $where;
	}

}
?>