<?php
/** Image repository */
$conf['IMG_REP_ROOT_LARGE'] = $_SERVER['DOCUMENT_ROOT'] . "mqs/img/large/";
$conf['IMG_REP_ROOT_THUMB'] = $_SERVER['DOCUMENT_ROOT'] . "mqs/img/thumb/";

/** Database Config*/
$conf['DATABASE_HOST'] 	= "localhost";
$conf['DATABASE_LOGIN'] = "root";
$conf['DATABASE_PASS'] 	= "mobil";
$conf['DATABASE_NAME']  = "mqsweb";

/** Logs Config*/
$conf['LOGS_ROOT'] = $_SERVER['DOCUMENT_ROOT'] . "mqs/logs/run.log";

?>