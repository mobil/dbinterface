<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
  */

@include_once("../inc/db.php");
@include_once("../inc/guimanager.php");
@include_once("../inc/user.php");

	$prefix= $_ENV['DOCUMENT_ROOT'] . "/dbinterface/data/";
	
	$reqLog = false;
	if($reqLog)
	{
		/*
		$myFile = "richiesta.txt";
		$fh = fopen($myFile, 'w') or die("can't open file");
		
		fwrite($fh, $stringData);
		$stringData = print_r($_POST, true);	
		fwrite($fh, $stringData);
		fclose($fh);
		*/
	
		$nick="param";
		$stringData =  "ti mostro i parametri passati via POST: \r\n";
		debug($nick,$stringData);
		
		$stringData = print_r($_POST, true);	
		debug($nick,$stringData);
		
		$stringData = "\r\n\r\nti mostro i parametri passati via GET: \r\n";
		debug($nick,$stringData);
		
		$stringData = print_r($_GET, true);
		debug($nick,$stringData);
		
		$stringData = "\r\n\r\nti mostro i parametri della REQUEST: \r\n";
		debug($nick,$stringData);
		
		$stringData = print_r($_REQUEST, true);	
		debug($nick,$stringData);
	}


	
	dispatcher();
	
	function authentication()
	{
		//session_start();
		$autorized = -1;
		/*if(isset($_SESSION["user"]))
		{
		 	$user = $_SESSION["user"];
			$autorized = 1;	
		}
		else
		{*/
			 if(isset($_REQUEST["login"]) && isset($_REQUEST["pass"]) )
			 {
			 	$user = new User();
				$user->username= $_REQUEST["login"];
				$user->password= $_REQUEST["pass"];
				$autorized= $user->authenticate();
				if($autorized)
				{
					$ip=$_SERVER['REMOTE_ADDR'];
					//$idSessione=session_id();
					$idSessione="ignorare";
					$user->ip= $ip;
					$user->session=$idSessione;
					
					$dbman = new db();
					$errorString="";
					$query="UPDATE `utenti` SET session='$idSessione' , ip='$ip'  WHERE username='$user->username';";
					$dbman->executeQuery($query, $errorString);				
					//$_SESSION["user"]= $user;
					echo $errorString;
				}
				else
				{
					echo "errore autenticazione";
				}
	
			 }
			 else
			 {
			 	echo "errore ... login assente";
			 }
		/*}*/	
		return $autorized;	
	}
	
	function dispatcher()
	{
		$resp="ciao";
				
		/***
		 * Richiesta per cataloghi
		 */
		 if(isset($_REQUEST["catalogId"])) 	
			$resp = richiestoUnCatalogo($_REQUEST["catalogId"]); 
		 elseif(isset($_REQUEST["productId"]))
		 {
		 	$resp = richiestoUnProdotto($_REQUEST["productId"]); 
		 }		 
		 elseif(isset($_REQUEST["service"]))
		 {
		 	switch($_REQUEST["service"])
		 	{
		 		case "search": //ricerca la parola che mi arriva come parametro
		 			$resp = search($_REQUEST["searchKey"]);
		 			break;
		 			
		 		case "catalogList": //DEPRECATED
		 			$resp = getCatalogList();
		 			break;
		 		case "markList":
		 			$resp = getMarksList();
		 			break;
		 		case "promoList": //DEPRECATED
		 			$resp = getPromoList();
		 			break;
		 		case "allCatalogList":		 			
		 			$resp = getCataAndPromoList();
		 			break;
				case "gui":
					if(authentication() == 1)
						$resp = getGuiManagerResponse();
					break;			
				/*case "logout":
					$resp = logOut();
					break;
				case "login":
				    $resp = "OK"; */
		 	}
		 } 
		 elseif(isset($_REQUEST["deleteDataId"]))
		 {
		 	global $prefix;
		 	$pos = strpos($_REQUEST["deleteDataId"], "/");
		 	if($pos === false)
		 	{
		 		$filepath =  $prefix . $_REQUEST["deleteDataId"];
		 		unlink($filepath);		 		
			}
			else
				debug("param.dispatcher", "Tentativo di attacco da parte dell'indirizzo IP  ". getClientIpAddress());

			 $resp = "";
		 }
		 
		if($resp != "") 
			presentation($resp);
	} 
	
	
	/** 
	 * 
	 * @param $data la stringa di stampa 
	 * @return the file path or the param string 
	 */
	function presentation(&$stringData)
	{
		if(isset($_REQUEST["stream"]))
			echo $stringData;
		else
			writeFile($stringData);		 
	}
	
	function writeFile(&$stringData)
	{
	  //echo $_ENV['PATH'];
	  global $prefix;
	
	  $casualName = rand();
	  $id =  $casualName . ".xml";
	  $filepath =  $prefix . $id;	  
	
	  $fh = fopen($filepath, 'w') or die("can't open file");
	  fwrite($fh, $stringData);  
	  fclose($fh);
	  
	  echo  $id;
	}
	
	function flickrMessage($inner, $type)
	{
		$prefix="<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?> <rsp>";
		$postfix="";
		switch($type)
		{
			case "catalog":
				$prefix .= "<photos page=\"1\" pages=\"1\" perpage=\"100\" total=\"2\">";
				$postfix .= "</photos>";
				break;
	
			case "list":
				$prefix .= "<list>";
				$postfix .= "</list>";	
			case "product":
				break;
					
		}
		$postfix .="</rsp>";
		
		return $prefix . $inner . $postfix;
	}
	
	function richiestoUnCatalogo($catalogId)
	{
		$dbman = new db();
		
	 	 	
	 	$query = 	" SELECT p.id, p.nome, p.img ".
					" FROM `prodoCataloghi` pc, `prodotti` p ".
					" WHERE p.id = pc.idProd " .
					" AND pc.idCatalogo =" . $catalogId;	
		$dumpError = "";
		$result = $dbman->executeQuery($query, $dumpError); 	
		$response = "";
		if(mysql_num_rows($result)>0)
		{
			//<photo id="518146379" title="Pre-wedding henna" path="" />
			$subFrags= array(
				"uno"=>  "<photo id=\"",	 
				"due"=>"\" title=\"", 
				"tre"=> "\" path=\"",
				"quattro"=>"\" ></photo>" 
			);
			
			while($row=mysql_fetch_array($result))
			{
				
				$response .= $subFrags["uno"] . $row["id"] .
					     $subFrags["due"] . $row["nome"] .
					     $subFrags["tre"] . $row["img"] .
					     $subFrags["quattro"];
	
			}//end while annidato
		}
		
		return flickrMessage($response, "catalog");	
	
	}
	
	
	function richiestoUnProdotto($productId=0)
	{
		$dbman = new db();
		$dumpError = "";
		
		$marchiS="";
		$tagsS="";
		$tipoS="";
		$catalogoS="";
	
		/***
		 *  Raccolgo le informazioni sui marchi del prodotto
		 */	
		$query = " SELECT m.* "  .
				 " FROM  marchi m, prodoMarchi pm " .
				 " WHERE pm.idProd = " . $productId  .
				 " AND pm.idMarchio = m.idMarchio";	
		$result = $dbman->executeQuery($query, $dumpError);	
		if(mysql_num_rows($result)>0)
		{
			//<marchio idMarchio="" nomeMarchio="" img="" ></marchio>
			$subFrags= array(
				"uno"=>  "<marchio idMarchio=\"",	 
				"due"=>"\" nomeMarchio=\"", 
				"tre"=> "\" img=\"",
				"quattro"=>"\" ></marchio>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$marchiS .= $subFrags["uno"] . $row["idMarchio"] .
					     $subFrags["due"] . $row["nomeMarchio"] .
					     $subFrags["tre"] . $row["img"] .
					     $subFrags["quattro"];
			}//end while annidato
		}
		
		
		/***
		 *  Raccolgo le informazioni sui tags del prodotto
		 */
		$query = " SELECT t.* " .
				 " FROM  tags t, prodoTags pt " .
				 " WHERE pt.idProd = ". $productId  . 
				 " AND pt.idTags = t.idTags";
		$result = $dbman->executeQuery($query, $dumpError);	
		if(mysql_num_rows($result)>0)
		{   //                          idTags       nomeTag
			//<tags><tag id="25679-2347366319-9497" >newyear</tag></tags>
			$subFrags= array(
			    "zero"=> "<tags>",
				"uno"=>  "<tag id=\"",	 
				"due"=>"\" >", 
				"tre"=> "</tag>",
				"quattro"=>"</tags>" 
			);		
			$tagsS .= $subFrags["zero"];		
			while($row=mysql_fetch_array($result))
			{			
				$tagsS .= $subFrags["uno"] . $row["idTags"] .
					     $subFrags["due"] . $row["nomeTag"] .
					     $subFrags["tre"];
			}//end while annidato
			$tagsS .= $subFrags["quattro"];
		}
			
		
		/***
		 *  Raccolgo le informazioni sul tipo associato al prodotto
		 */
		$query = " SELECT t.* " .
				 " FROM  tipi t, prodoTipi pt " .
				 " WHERE pt.idProd = ". $productId  . 
				 " AND pt.idTipo = t.idTipo";	
		//<tipo idTipo="" nomeTipo="" path="" ></tipo>
		$result = $dbman->executeQuery($query, $dumpError);	
		if(mysql_num_rows($result)>0)
		{
			$subFrags= array(
				"uno"=>  "<tipo idTipo=\"",	 
				"due"=>"\" nomeTipo=\"", 
				"tre"=> "\" path=\"",
				"quattro"=>"\" ></tipo>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$tipoS .= $subFrags["uno"] . $row["idTipo"] .
					     $subFrags["due"] . $row["nomeTipo"] .
					     $subFrags["tre"] . $row["path"] .
					     $subFrags["quattro"];
			}//end while annidato
		}
		
		
		/***
		 *  Raccolgo le informazioni sul catalogo a cui appartiene il prodotto
		 */
		$query = " SELECT c.idCatalogo, c.nomeCatalogo " .
				 " FROM  cataloghi c, prodoCataloghi pc " .
				 " WHERE pc.idProd = ". $productId  . 
				 " AND pc.idCatalogo = c.idCatalogo ";
		//<catalogo idCatalogo="" nomeCatalogo=""></catalogo>
		$result = $dbman->executeQuery($query, $dumpError);	
		if(mysql_num_rows($result)>0)
		{
			$subFrags= array(
				"uno"=>  "<catalogo idCatalogo=\"",	 
				"due"=>"\" nomeCatalogo=\"",			
				"tre"=>"\" ></catalogo>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$catalogoS .= $subFrags["uno"] . $row["idCatalogo"] .
					     $subFrags["due"] . $row["nomeCatalogo"] .
					     $subFrags["tre"];
			}//end while annidato
		}
		
		
		
		/***
		 * Raccolgo le informazioni sul prodotto
		 */ 	 	
	 	$query = 	" SELECT p.* ".
					" FROM `prodotti` p ".
					" WHERE p.id =  " .  $productId;
		$result = $dbman->executeQuery($query, $dumpError); 	
		$response = "";
		if(mysql_num_rows($result)>0)
		{
			//<photo id="518146379"><title></title><description></description>$tags</photo>
			$subFrags= array(
				"uno"=>  "<photo id=\"",	 
				"due"=>"\" ><title>", 
				"tre"=> "</title><description>",
				"tre.uno"=> "</description>",
				"tre.due"=> "<price>",
				"tre.tre"=> "</price>",
				"tre.quattro"=> "<promoprice>",
				"tre.cinque"=> "</promoprice>",
				"quattro"=>"<urls><url>",  		
			    "cinque"=>"</url></urls>",
			    "sei"=>"</photo>"		
				 
			);		
			while($row=mysql_fetch_array($result))
			{			
				$response .= $subFrags["uno"] . $row["id"] .
					     $subFrags["due"] . iconv("UTF-8", "ISO-8859-1", $row["nome"]) .
					     $subFrags["tre"] . iconv("UTF-8", "ISO-8859-1", $row["descrizione"])   . $subFrags["tre.uno"] ;					     
			if($row["prezzo"] != "0.00")		      
				$response .= $subFrags["tre.due"] . $row["prezzo"].  $subFrags["tre.tre"] ;
				
			if($row["prezzopromozionale"] != "0.00")
				$response .= $subFrags["tre.quattro"] . $row["prezzopromozionale"].  $subFrags["tre.cinque"] ;
				
			$response .= $subFrags["quattro"] . $row["img"] . $subFrags["cinque"];
	
			}//end while annidato
			$response .=  $marchiS . $tagsS . $tipoS . $catalogoS . $subFrags["sei"];
			//Valutare il caso composizione con la query su tutti gli ID che richiama
		}
		
		return flickrMessage($response, "product");
	 	
					
	}
	 
	
	function getCatalogList()
	{
		$query= " SELECT idCatalogo, nomeCatalogo " .
				" FROM `cataloghi`" .
				" WHERE `tipoCatalogo` != 'p'";
		$dbman = new db();
		$dumpError = "";
		$response = "";
		$result = $dbman->executeQuery($query, $dumpError);
		if(mysql_num_rows($result)>0)
		{
			$subFrags= array(
				"uno"=>  "<item id=\"",	 
				"due"=>"\" nome=\"",			
				"tre"=>"\" service=\"". $_REQUEST["service"] . "\" ></item>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$response .= $subFrags["uno"] . $row["idCatalogo"] .
					     $subFrags["due"] . $row["nomeCatalogo"] .
					     $subFrags["tre"];
			}//end while annidato
		}
		return flickrMessage($response, "list");
	}
	
	function getPromoList()
	{
		$query= " SELECT idCatalogo, nomeCatalogo, dtInizValid, dtFineValid ".
			" FROM `cataloghi` " .
			" WHERE `tipoCatalogo` = 'p' ";
		//TODO: va fatta una query + raffinata che limiti la visibilità a quelle promozioni attive
		$dbman = new db();
		$dumpError = "";
		$response = "";
		$result = $dbman->executeQuery($query, $dumpError);
		if(mysql_num_rows($result)>0)
		{
			$subFrags= array(
				"uno"=>  "<item id=\"",	 
				"due"=>"\" nome=\"",			
				"tre"=>"\" service=\"". $_REQUEST["service"] . "\" ></item>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$response .= $subFrags["uno"] . $row["idCatalogo"] .
					     $subFrags["due"] . $row["nomeCatalogo"] .
					     $subFrags["tre"];
			}//end while annidato
		}
		return flickrMessage($response, "list");
	}
	
	function getCataAndPromoList()
	{
	    $query = 	" SELECT idCatalogo, nomeCatalogo, dtInizValid, dtFineValid, tipoCatalogo " .
			" FROM `cataloghi` " .
			" WHERE (`tipoCatalogo` = 'p' AND dtInizValid <= CURDATE() AND dtFineValid >= CURDATE() )" .
			" OR `tipoCatalogo` != 'p' " .
			" ORDER BY `tipoCatalogo`, `dtInizValid` DESC " ;
	    
	    $dbman = new db();
	    $dumpError = "";
	    $response = "";
	    $result = $dbman->executeQuery($query, $dumpError);
	    if(mysql_num_rows($result)>0)
	    {
		    $subFrags= array(
			    "uno"=>  "<item id=\"",	 
			    "due"=>"\" nome=\"",
			    "tre"=>"\" tipocatalogo=\"",
			    "quattro"=>"\" service=\"". $_REQUEST["service"] . "\" ></item>" 
		    );
		    while($row=mysql_fetch_array($result))
		    {
			$nome = $row["nomeCatalogo"]; //TODO: <s.mazzurco> qui va visto se gestire il nome delle promo con la data
			$response .= $subFrags["uno"] . $row["idCatalogo"] .
					      $subFrags["due"] . $nome .
					      $subFrags["tre"] . $row["tipoCatalogo"] .
					      $subFrags["quattro"];
		    }//end while annidato
	    }
	    return flickrMessage($response, "list");
	}
	
	function getMarksList()
	{
		$query= " SELECT idMarchio, nomeMarchio, img " .
				" FROM marchi ";
		$dbman = new db();
		$dumpError = "";
		$response = "";
		$result = $dbman->executeQuery($query, $dumpError);
		if(mysql_num_rows($result)>0)
		{
			$subFrags= array(
				"uno"=>  "<item id=\"",	 
				"due"=>"\" nome=\"",
				"tre"=>"\" img=\"",			
				"quattro"=>"\" service=\"". $_REQUEST["service"] . "\" ></item>" 
			);
			while($row=mysql_fetch_array($result))
			{			
				$response .= $subFrags["uno"] . $row["idMarchio"] .
					     $subFrags["due"] . $row["nomeMarchio"] .
					     $subFrags["tre"] . $row["img"] .
					     $subFrags["quattro"];
			}//end while annidato
		}
		return flickrMessage($response, "list");	
	}
	
	
	function search($searchKey)
	{
		$flag_prod=true;
		$flag_catalog=true;
		$flag_tipi=true;
		$flag_tag=true;
		$flag_marks=true;
		
		$pieces = explode("::", $searchKey);
		if(count($pieces) == 2)
		{
			switch($pieces[0])
			{
				case "tag":
					$flag_prod = false;
					$flag_catalog=false;
					$flag_tipi=false;
					$flag_tag=true;
					$flag_marks=false;
					$searchKey = $pieces[1];
					break;
				case "mark":	
					$flag_prod = false;
					$flag_catalog=false;
					$flag_tipi=false;
					$flag_tag=false;
					$flag_marks=true;
					$searchKey = $pieces[1];
					break;	
			}
		}
		$respAr = array(); //[productId] => (tupla)
		
		
		if($flag_prod)
		{
			#Fase 1  ricerca su prodotti
			$query = " SELECT p.* " .
					 " FROM  prodotti p, prodoCataloghi pc ".
			         " WHERE p.id = pc.idProd ".
					 " AND ( p.nome LIKE '%$searchKey%' ". 
					 " OR p.descrizione LIKE '%$searchKey%' )";
			searchInner($query, $respAr);
			
		}
		if($flag_catalog)
		{
			#Fase 2  ricerca su cataloghi  se
			$query = " SELECT p.*  " .
					 " FROM  prodotti p, cataloghi c, prodoCataloghi pc " .
					 " WHERE c.nomeCatalogo LIKE '%$searchKey%' " . 
					 " AND pc.idCatalogo = c.idCatalogo " .
					 " AND pc.idProd = p.id  ";
			searchInner($query, $respAr);
		
		}
		if($flag_tipi)
		{
			#Fase 3  ricerca sui tipi
			$query = " SELECT p.* " .
					 " FROM  prodotti p, tipi t, prodoTipi pt, prodoCataloghi pc  " .
					 " WHERE p.id = pc.idProd ".
					 " AND t.nomeTipo LIKE '%$searchKey%'  " .
					 " AND pt.idTipo = t.idTipo " .
					 " AND pt.idProd = p.id " ;
			searchInner($query, $respAr);
		}
		if($flag_tag)
		{
			#Fase 4 ricerca per Tag
			$query = " SELECT p.* " .
					 " FROM  prodotti p, tags t, prodoTags pt, prodoCataloghi pc  " .
					 " WHERE p.id = pc.idProd ".
					 " AND t.nomeTag LIKE '%$searchKey%'  " .
					 " AND pt.idTags = t.idTags " .
					 " AND pt.idProd = p.id " ;
			searchInner($query, $respAr);
		}
		if($flag_marks)
		{
			#Fase 5 Ricerca per marchi
			$query = " SELECT p.*  " .
					 " FROM  prodotti p, marchi m, prodoMarchi pm, prodoCataloghi pc  " .
					 " WHERE p.id = pc.idProd ".
					 " AND m.nomeMarchio LIKE '%$searchKey%'  " .
					 " AND pm.idMarchio = m.idMarchio " .
					 " AND pm.idProd = p.id " ;
			searchInner($query, $respAr);		
		}
		
		//Gestione dell'array
		$subFrags= array(
				"uno"=>  "<photo id=\"",	 
				"due"=>"\" title=\"", 
				"tre"=> "\" path=\"",
				"quattro"=>"\" ></photo>" 
			);
			
			foreach ($respAr as $key=>$row) 
			{
				$response .= $subFrags["uno"] . $row["id"] .
					     $subFrags["due"] . $row["nome"] .
					     $subFrags["tre"] . $row["img"] .
					     $subFrags["quattro"];	
			}
				
			return flickrMessage($response, "catalog");	
	}
	
	function searchInner($query, &$arr)
	{
		$dbman = new db();
		$dumpError = "";
		$result = $dbman->executeQuery($query, $dumpError);	
		if(mysql_num_rows($result)>0)
		{
			while($row=mysql_fetch_array($result))
			{
				$key= $row["id"];
				$arr[$key]= $row;
			}
		}
	}
	
	function getGuiManagerResponse()
	{
	  if(isset($_REQUEST["xmlmessage"]))
	  {
	    
	    $guiman = new guimanager();
	    //Correggo alcuni eventuali errori di url encoding
	    $str = $_REQUEST["xmlmessage"];
	    $str = str_replace("\\\"", "\"", $str);
		$str = str_replace("\'", "'", $str);
	    return $guiman->process($str);
	  }
	  else
	    return "Errore mi manca il messaggio xml da parsare!";
	
	}
	
	/*
	function logOut()
	{
		$_SESSION =  array(); //. (array[string]mixed) .
		setcookie(session_name(), '', 1, '/');
		session_destroy();
		return "OK!";		
	}
	*/
 
?>