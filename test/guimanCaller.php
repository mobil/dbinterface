<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include("../inc/guimanager.php");
$req = "<request>" .
 			"<op id=\"crea\" >" .
  				"<item table=\"tags\" nomeTag=\"untag\" />" .
				"<item table=\"tags\" nomeTag=\"duetag\" />" .
  				"<item table=\"tipi\" idTipo=\"3\" nomeTipo=\"tipobanana\" path=\"1.3\" />" .
				"<item table=\"cataloghi\" nomeCatalogo=\"provaInser\" tipoCatalogo=\"d\"  dtInizValid=\"1900-01-01\" dtFineValid=\"9999-12-12\" />".
				"<item table=\"marchi\" nomeMarchio=\"marchioprova\" img=\"/alessandro/floridafamily.png\" />".
				"<item table=\"prodotti\" genere=\"p\" nome=\"foto prova\" img=\"/alessandro/pluto.png\"  prezzo=\"-1.00\" prezzopromozionale=\"-1.00\" descrizione=\"una descrizione di prova di un prodotto\" presente=\"0\" />".
				"<item table=\"prodotti\" genere=\"c\" nome=\"foto prova composizione\" img=\"/alessandro/pluto2.png\"  prezzo=\"-1.00\" prezzopromozionale=\"-1.00\" descrizione=\"una descrizione di una composizione\" presente=\"0\" />".
				"<item table=\"prodoInComposizione\" idProd=\"2\" idComposizione=\"3\" />" .
				"<item table=\"prodoInComposizione\" idProd=\"6\" idComposizione=\"3\" />" .
				"<item table=\"prodoCataloghi\" idProd=\"2\" idCatalogo=\"3\" />" .
				"<item table=\"prodoMarchi\" idProd=\"2\" idMarchio=\"1\" />" .
				"<item table=\"prodoTags\" idProd=\"2\" idTags=\"3\" />" .
				"<item table=\"prodoTags\" idProd=\"2\" idTags=\"1\" />" .
				"<item table=\"prodoTipi\" idProd=\"2\" idTipo=\"3\" />" .
 			"</op>".
			"<op id=\"aggiorna\" >" .
				"<item table=\"prodotti\" where=\"idProd='2'\" genere=\"p\" nome=\"foto riprova\" descrizione=\"una descrizione ridefinita di prova di un prodotto\"  />".
 			"</op>".
			"<op id=\"cancella\" >" .
				"<item table=\"prodoTipi\" where=\"idProd='2' and idTipo='3'\"  />" .
				"<item table=\"tipi\" where=\"idTipo='3'\"  />" .
 			"</op>".
			"<op id=\"view\" >" .
				"<item table=\"prodoTipi\" where=\"idProd='2'\"  />" .
				"<item table=\"tipi\"  />" .
 			"</op>".
		"</request>";
$req = "<request>".
			//"<op id='view'><item table='tipi' /></op>".
			"<op id='raw'><item q=\"SELECT max(idTipo)+1 as newId FROM `tipi` \" /></op>" .			
		"</request>"; 
$req = "<request>" .		
	"<op id=\"view\" >" .
		"<item table=\"prodoTipi\"   />" .
		"<item table=\"tipi\"  />" .
	"</op>".
"</request>";
/** where=\"idProd='2'\" */
$guiman = new guimanager();
echo $guiman->process($req);

?>