<?php 
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/


 $catalogoInizialized=0;
 
 //Creates XML string and XML document using the DOM 
 $dom = new DomDocument('1.0'); 

 //add root - <books> 
 $books = $dom->appendChild($dom->createElement('books')); 
 
 $books2 = $books->appendChild($dom->createAttribute('id')); 
 $books2->appendChild($dom->createTextNode('books')); 
 $books->setIdAttribute("id", true);
 
 //add <book> element to <books> 
 $book = $books->appendChild($dom->createElement('book')); 

 //add <title> element to <book> 
 $title = $book->appendChild($dom->createElement('title')); 

 //add <title> text node element to <title> 
 $title->appendChild($dom->createTextNode('Great American Novel')); 

 putCatalog($dom, '12', 'altea', '01/01/2009', '30/06/2009');
 putProduct($dom, '12', '1', 'pippo.png', 'pippo', '150.00', '149.99', 'prodotto interessante','true');

 //generate xml 
 $dom->formatOutput = true; // set the formatOutput attribute of 
                            // domDocument to true 

 // save XML as string or file 
 $test1 = $dom->saveXML(); // put string in test1
 if(isset($_REQUEST["xml"]))
 {
 	$tmp = str_replace("\\\"", "\"" , $_REQUEST["xml"]) . "\n</books>";
 	$test1 = str_replace("</books>", $tmp , $test1); 	
 }
 //$dom->save('test1.xml'); // save as file 
 echo $test1;
 
print_r($_POST);
print_r($_REQUEST); 
//$post_vars = '';
//if ($_REQUEST)
//{
//	foreach($_REQUEST as $key=>$value)
//	{
//		$value = urlencode(stripslashes($value));
//		$post_vars .= " $key=$value";
//	}
//	//$post_vars = substr($post_vars, 1); //remove the first "&" from the string
//}
//echo 'REQUEST variables in string: --|'. $post_vars . '|--';


//$crypt = crypt($test1);
//echo '<br/> Dopo la crittografia: ' . $crypt;




function inizializeCatalogs(&$dom)
{
	$res = $dom->getElementById('books');
	if($res == NULL)
	{
		
		$res = $dom->appendChild($dom->createElement('books')); 
		$books2 = $res->appendChild($dom->createAttribute('id')); 
		$books2->appendChild($dom->createTextNode('books'));
		$res->setIdAttribute("id", true); 
	}
	return $res;
}

function putCatalog(&$dom, $cod, $nome, $dtInizValid, $dtFineValid)
{
	$collector= inizializeCatalogs($dom);
	
	$catalog = $collector->appendChild($dom->createElement('catalog')); 
	
	//id same cod
	$catalog2 = $catalog->appendChild($dom->createAttribute('id')); 
 	$catalog2->appendChild($dom->createTextNode('catalog_' . $cod));
 	$catalog->setIdAttribute("id", true);
 	
 	//cod
 	$code = $catalog->appendChild($dom->createElement('cod'));
 	$code->appendChild($dom->createTextNode($cod));
 	
 	//name
 	$name = $catalog->appendChild($dom->createElement('name'));
 	$name->appendChild($dom->createTextNode($nome));
 	
 	//dtInizValid
 	$dtInizValidX = $catalog->appendChild($dom->createElement('dtInizValid'));
 	$dtInizValidX->appendChild($dom->createTextNode($dtInizValid));
	
 	//dtFineValid
 	$dtFineValidX = $catalog->appendChild($dom->createElement('dtFineValid'));
 	$dtFineValidX->appendChild($dom->createTextNode($dtFineValid));	
}


function putProduct(&$dom, $catalogCod, $cdProd, $imgpath, $name, $prezzo, $prezzopromozionale, $descrizione, $inMagazzino)
{
	$catalog = $dom->getElementById('catalog_' . $catalogCod);
	
	$product = $catalog->appendChild($dom->createElement('product'));
	
 	//cdProd
 	$code = $product->appendChild($dom->createElement('cdProd'));
 	$code->appendChild($dom->createTextNode($cdProd));
 	
 	//img
 	$imgpathX = $product->appendChild($dom->createElement('imgpath'));
 	$imgpathX->appendChild($dom->createTextNode($imgpath));
 	
 	//name
 	$nameX = $product->appendChild($dom->createElement('name'));
 	$nameX->appendChild($dom->createTextNode($name));


 	//prezzo
 	$prezzoX = $product->appendChild($dom->createElement('prezzo'));
 	$prezzoX->appendChild($dom->createTextNode($prezzo));

 	//prezzopromozionale
 	$prezzopromozionaleX = $product->appendChild($dom->createElement('prezzopromozionale'));
 	$prezzopromozionaleX->appendChild($dom->createTextNode($prezzopromozionale));

 	//descrizione
 	$descrizioneX = $product->appendChild($dom->createElement('descrizione'));
 	$descrizioneX->appendChild($dom->createTextNode($descrizione));

 	//inMagazzino
 	$inMagazzinoX = $product->appendChild($dom->createElement('inMagazzino'));
 	$inMagazzinoX->appendChild($dom->createTextNode($inMagazzino));
	
}

 ?>

