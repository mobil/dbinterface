<?php
/*
 	Copyright (C) 2009 Salvatore Mazzurco <s4lv00@gmail.com>
 	
 	This file is part of Mobil Quattro Sud CMS.

    Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.
    
    
    For license details read COPYING.txt .
	For all other info read README.txt .
	
*/

@include("../inc/db.php");


function queryTest()
{
	$dbman = new db();
	$query = "SELECT * FROM `cataloghi`";
	$dumpError = "";
	$result = $dbman->executeQuery($query, $dumpError);

	if ($dumpError != "")			
		die($dumpError);

	$response = "<table border=\"1\"><tr><th>id Catalogo</th><th>nome Catalogo</th><th>tipo Catalogo</th><th>Data Inizio Validit&aacute;</th><th>Data Fine Validit&aacute; </th></tr>";
	
	if(mysql_num_rows($result)>0)
	{

		$subFrags= array(
			"start"=>  "<tr><td>",	 
			"iter"=>"</td><td>", 
			"end"=>"</td></tr>\r\n" 
		);
		
		while($row=mysql_fetch_array($result))
		{
			
			$response .= $subFrags["start"] . $row["idCatalogo"] .
				     $subFrags["iter"] . $row["nomeCatalogo"] .
				     $subFrags["iter"] . $row["tipoCatalogo"] . 
				     $subFrags["iter"] . $row["dtInizValid"] .
				     $subFrags["iter"] . $row["dtFineValid"] .
				     $subFrags["end"];

		}//end while annidato
	}
	$response .= "</table>";
	return $response; 
}

function paintPage($body)
{
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
	<head>
		<title>dbCaller</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 		<meta name="author" content="S4lv0"/>
		<meta name="description" content="test del database"/>
 		<meta name="keywords" content=""/>		
	</head>	
	<body>
<? 
print $body;
?>
	</body>
</html>
<? 
}

$body = queryTest();
paintPage($body);
?>
